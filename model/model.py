from tensorflow import keras


def generate_model():
    model = keras.Sequential([
        keras.layers.Dense(units=36, input_shape=(9,), activation="relu"),
        keras.layers.Dense(units=36, activation="relu"),
        keras.layers.Dense(units=36, activation="relu"),
        keras.layers.Dense(units=9),
    ])

    model.compile(optimizer='adam', loss='mean_squared_error', metrics=['mean_squared_error'])
    return model


def train(x, y):
    model = generate_model()
    model.summary()
    model.fit(x, y, epochs=100)
    model.save_weights('./checkpoints/tic_tac_toe')


def load_model():
    model = generate_model()
    model.load_weights('./checkpoints/tic_tac_toe').expect_partial()
    return model
