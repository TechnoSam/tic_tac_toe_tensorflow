# tic_tac_toe_tensorflow

An experiment in training a neural network to play Tic Tac Toe

## Structure

The application consists of three parts.

1. Collect training data by playing games.
2. Train a model with the data collected in part 1.
3. Use the model to predict the best move.

### Collect

```text
usage: collect_tic_tac_toe.py [-h] [-r [N]]

Collect data for training a neural network from playing games

options:
  -h, --help            show this help message and exit
  -r [N], --random [N]  Play N random games automatically
```

The game data is aggregated into `game_histories.json`. No duplicate games will be recorded. Games are stored as a
series of integers representing moves followed by `"X"` if X won, `"O"` if O won, and `"~"` if it was a tie. All records
are 10 characters long, unused move slots are filled with spaces.

### Train

```text
usage: train_tic_tac_toe.py
```

I selected a model with nine input units and nine output units. The input is a one-hot representations of the board.
The output is a quality rating for each of the squares, 0-1, higher means the move is better.

The model is trained to play as X, so it only looks for moves made by X to evaluate.

For input, `1` means the square is occupied by an "X", `-1` means the square is occupied by an "O", and `0` means it
is unoccupied.

There are three hidden layers with 36 units each. I don't have a rationale for choosing this, I just experimented
until the behavior was good enough. More work could be done here.

I chose a mean-squared loss function rather than a category based loss function because I'm producing a probability
field. I want floating point outputs, not integer. Maybe this is wrong, I'm not super confident about this choice.

Lastly the model is trained for 100 epochs. Again, no real rationale here, it seems common to do, and I'm happy with
the results.

The model is saved to `./checkpoints`.

### Predict

```text
usage: predict_tic_tac_toe.py
```

This script will let you play a game against an AI driving by the trained model.

For now, the AI only knows how to play as X, and the initial board state is the same every time, so that means it
always opens with the same move. It may be helpful to choose the first move for the AI to make it more fun.

## Comments

Collecting enough data proved to be harder than I thought it would be. It needs a lot of data to succeed. I ended up
needing 120,000 games to get the performance I wanted. I had to randomly generate those games, which probably doesn't
help the numbers. I bet if I used real games I wouldn't need that many. But even collecting 1000 games at this scale
would be difficult.

I am happy with the performance from that training data. The AI does not win every game that it could, but it never
loses. I didn't set out to build the perfect AI (Tic Tac Toe is trivially solved, so a neural network
doesn't make much practical sense here anyway), I just wanted to learn how the libraries work and get some experience
with the technology, which I think I have satisfied, for now at least.
