import json
import os
import argparse

from tic_tac_toe import Game


class GameHistories:
    def __init__(self):
        self.histories: set[str] = set()
        if not os.path.exists('game_histories.json'):
            with open('game_histories.json', 'w') as f:
                json.dump({}, f)
        with open('game_histories.json', 'r') as f:
            existing_data = json.load(f)
        if "histories" in existing_data:
            self.histories = set(existing_data["histories"])

    def add(self, history):
        self.histories.add(str(history))

    def update(self, histories):
        self.histories.update([str(h) for h in histories])

    def count(self):
        return len(self.histories)

    def save(self):
        with open('game_histories.json', 'w') as f:
            json.dump({'histories': list(self.histories)}, f)


def play():
    game = Game()
    (_, history) = game.play()
    return [history]


def play_random(random_games: int):
    new_histories = []
    for game_index in range(random_games):
        game = Game()
        (result, history) = game.play_random()
        print(result)
        new_histories.append(history)
    return new_histories


def main(random_games: int = None):
    histories = GameHistories()
    if random_games:
        histories.update(play_random(random_games))
    else:
        histories.update(play())
    print(f'Currently tracking {histories.count()} games')
    histories.save()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collect data for training a neural network from playing games')
    parser.add_argument('-r', '--random', metavar='N', nargs='?', const=1, type=int,
                        help='Play N random games automatically')
    args = parser.parse_args()
    main(random_games=args.random)
