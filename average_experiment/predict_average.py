from tensorflow import keras


def main():
    model = keras.Sequential([
        keras.layers.Dense(units=4, input_shape=(2,)),
        keras.layers.Dense(units=4),
        keras.layers.Dense(units=1),
    ])
    model.load_weights('./checkpoints/average').expect_partial()

    print(model.predict([[300, 300]]))


if __name__ == '__main__':
    main()
