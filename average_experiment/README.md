# Average Experiment

Before I could get anywhere with complex IO, I needed to understand how to work the tools, so I built this tiny
neural network to learn how to compute the average of two numbers. It seems to work pretty well.

It is organized similar to the main project, a train step and a predict step. There is no collect step, the data
is generated immediately before training.
