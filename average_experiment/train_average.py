from tensorflow import keras
import random


def main():
    pairs = []
    for _ in range(100):
        pairs.append([random.randint(0, 1000), random.randint(0, 1000)])
    averages = [(p[0] + p[1]) / 2 for p in pairs]

    print(pairs[:10])
    print(averages[:10])

    model = keras.Sequential([
        keras.layers.Dense(units=4, input_shape=(2,)),
        keras.layers.Dense(units=4),
        keras.layers.Dense(units=1),
    ])

    model.compile(optimizer='adam', loss='mean_squared_error', metrics=['mean_squared_error'])
    model.summary()

    model.fit(pairs, averages, epochs=100)

    model.save_weights('./checkpoints/average')


if __name__ == '__main__':
    main()
