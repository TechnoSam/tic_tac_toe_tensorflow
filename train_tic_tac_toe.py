import json
from collections import defaultdict
from typing import Self

from model import train
from tic_tac_toe import History, Result


class InputBoard:
    CROSS = 1
    CIRCLE = -1
    UNOCCUPIED = 0

    def __init__(self, cells=None, next_move=None):
        self.cells = (self.UNOCCUPIED,) * 9 if cells is None else cells
        self.next_move = self.CROSS if next_move is None else next_move

    def __str__(self):
        return ','.join([str(c) for c in self.cells])

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, InputBoard):
            return False
        return self.cells == other.cells

    def __hash__(self):
        return hash(self.cells)

    def get_input_data(self):
        return [float(c) for c in list(self.cells)]

    def record_move(self, move: int) -> Self:
        cells_list = list(self.cells)
        cells_list[move] = self.next_move
        cells = tuple(cells_list)
        if self.next_move == self.CROSS:
            next_move = self.CIRCLE
        else:
            next_move = self.CROSS
        return InputBoard(cells, next_move)


def normalize(score: list[int]):
    min_score = min(score)
    max_score = max(score)
    return [0 if s == 0 else (s - min_score) / (max_score - min_score) for s in score]


def load_histories() -> list[History]:
    with open('game_histories.json', 'r') as f:
        data = json.load(f)
        return [History.from_str(h) for h in data["histories"]]


def create_training_data():
    histories = load_histories()

    board_evaluations: dict[InputBoard, [int]] = defaultdict(lambda: [0, 0, 0, 0, 0, 0, 0, 0, 0])
    for history in histories:
        if history.result == Result.TIE:
            continue
        intermediate_value = 1 if history.result == Result.CROSS_WIN else -1
        input_board = InputBoard()
        for move in history.moves:
            if input_board.next_move == input_board.CROSS:
                board_evaluations[input_board][move] += intermediate_value
            input_board = input_board.record_move(move)

    boards = [b.get_input_data() for b in board_evaluations.keys()]
    scores = [normalize(s) for s in board_evaluations.values()]

    return boards, scores


def main():
    boards, scores = create_training_data()
    train(boards, scores)


if __name__ == '__main__':
    main()
