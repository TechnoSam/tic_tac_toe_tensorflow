from enum import Enum, auto
from random import shuffle
from typing import Self
import re


class Player(Enum):
    CROSS = auto()
    CIRCLE = auto()

    def __str__(self):
        if self.name == self.CROSS.name:
            return 'Player with X'
        return 'Player with O'


class Result(Enum):
    NOT_FINISHED = auto()
    CROSS_WIN = auto()
    CIRCLE_WIN = auto()
    TIE = auto()

    def __str__(self):
        if self.name == self.NOT_FINISHED.name:
            return 'No winner yet!'
        if self.name == self.CROSS_WIN.name:
            return 'X Wins!'
        if self.name == self.CIRCLE_WIN.name:
            return 'O Wins!'
        return 'It\'s a tie!'

    def encode(self):
        if self.name == Result.CROSS_WIN.name:
            return 'X'
        elif self.name == Result.CIRCLE_WIN.name:
            return 'O'
        elif self.name == Result.TIE.name:
            return '~'
        else:
            return '.'

    @staticmethod
    def decode(string: str):
        if string == 'X':
            return Result.CROSS_WIN
        if string == 'O':
            return Result.CIRCLE_WIN
        if string == '~':
            return Result.TIE
        return Result.NOT_FINISHED


class Cell(Enum):
    EMPTY = auto()
    CROSS = auto()
    CIRCLE = auto()

    def __str__(self):
        if self.name == self.EMPTY.name:
            return ' '
        if self.name == self.CROSS.name:
            return 'X'
        return 'O'


class Turn:
    def __init__(self):
        self.player: Player = Player.CROSS

    def cell(self) -> Cell:
        if self.player == Player.CROSS:
            return Cell.CROSS
        return Cell.CIRCLE

    def result(self) -> Result:
        if self.player == Player.CROSS:
            return Result.CROSS_WIN
        return Result.CIRCLE_WIN

    def switch(self):
        if self.player == Player.CROSS:
            self.player = Player.CIRCLE
        else:
            self.player = Player.CROSS


HISTORY_PATTERN = re.compile('(\\d*)\\s*([XO~])')


class History:
    def __init__(self, moves=None, result=Result.NOT_FINISHED):
        self.moves: [int] = [] if moves is None else moves
        self.result: Result = result

    @classmethod
    def from_str(cls, string: str) -> Self:
        search = HISTORY_PATTERN.search(string)
        if not search:
            raise ValueError(f'Invalid string {string}')
        (moves, result) = search.groups()
        moves = [int(move) for move in list(moves)]
        result = Result.decode(result)
        return cls(moves, result)

    def record_move(self, index: int):
        self.moves.append(index)

    def record_result(self, result: Result):
        self.result = result

    def __str__(self):
        encoded_moves = ''.join(str(m) for m in self.moves)
        encoded_result = self.result.encode()
        return f'{encoded_moves: <9}{encoded_result}'


class State:
    NUM_ROWS: int = 3
    NUM_COLS: int = 3
    NUM_CELLS: int = NUM_ROWS * NUM_COLS
    WIN_LINES = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]

    def __init__(self):
        self.board: [Cell] = [Cell.EMPTY for _ in range(self.NUM_CELLS)]
        self.turn: Turn = Turn()
        self.history: History = History()

    def __str__(self):
        return f'     |     |     \n' \
               f'  {self.board[0]}  |  {self.board[1]}  |  {self.board[2]}  \n' \
               f' (0) | (1) | (2) \n' \
               f'-----+-----+-----\n' \
               f'     |     |     \n' \
               f'  {self.board[3]}  |  {self.board[4]}  |  {self.board[5]}  \n' \
               f' (3) | (4) | (5) \n' \
               f'-----+-----+-----\n' \
               f'     |     |     \n' \
               f'  {self.board[6]}  |  {self.board[7]}  |  {self.board[8]}  \n' \
               f' (6) | (7) | (8) \n'

    def player(self):
        return self.turn.player

    def place(self, index: int) -> Result:
        if index < 0 or index > self.NUM_CELLS - 1:
            raise IndexError(f'Invalid position: {index}')
        if self.board[index] != Cell.EMPTY:
            raise IndexError(f'Position {index} already occupied')
        self.history.record_move(index)
        self.board[index] = self.turn.cell()
        winner = self._winner()
        self.history.record_result(winner)
        self.turn.switch()
        return winner

    def _winner(self) -> Result:
        cell = self.turn.cell()
        for indexes in self.WIN_LINES:
            match = True
            for index in indexes:
                match = match and self.board[index] == cell
            if match:
                return self.turn.result()
        if self._empty_cell_count() == 0:
            return Result.TIE
        return Result.NOT_FINISHED

    def _empty_cell_count(self):
        empty_cell_count = 0
        for cell in self.board:
            if cell == Cell.EMPTY:
                empty_cell_count += 1
        return empty_cell_count


class Game:
    def __init__(self):
        self.state = State()

    def play(self):
        result: Result = Result.NOT_FINISHED
        while result == Result.NOT_FINISHED:
            print(self.state)
            print(f'{self.state.player()}, it\'s your turn.')
            while True:
                index = input('Enter a position to place your mark (0-8): ')
                try:
                    index = int(index)
                except ValueError:
                    print(f'Invalid input. "{index}" is not an integer')
                    continue
                try:
                    result = self.state.place(index)
                except IndexError as e:
                    print(f'Invalid input: {e}')
                    continue
                break
        print(self.state)
        print('Game Over!')
        print(result)
        return result, self.state.history

    def play_random(self):
        moves = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        shuffle(moves)
        result: Result = Result.NOT_FINISHED
        while result == Result.NOT_FINISHED:
            result = self.state.place(moves.pop())
        return result, self.state.history

    def play_turn(self):
        result: Result = Result.NOT_FINISHED
        print(f'Human, it\'s your turn.')
        while True:
            index = input('Enter a position to place your mark (0-8): ')
            try:
                index = int(index)
            except ValueError:
                print(f'Invalid input. "{index}" is not an integer')
                continue
            try:
                result = self.state.place(index)
            except IndexError as e:
                print(f'Invalid input: {e}')
                continue
            break
        return result

    def get_board(self):
        return self.state.board

    def play_turn_ai(self, index):
        return self.state.place(index)
