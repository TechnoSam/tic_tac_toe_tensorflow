from model import load_model
from tic_tac_toe import Game, Result, Cell


def nn_repr(cell: Cell):
    if cell == Cell.CROSS:
        return 1
    if cell == Cell.CIRCLE:
        return -1
    return 0


def main():
    model = load_model()

    game = Game()
    result = Result.NOT_FINISHED
    while result == Result.NOT_FINISHED:
        print(game.state)
        board = game.get_board()
        board = [[nn_repr(c) for c in board]]
        prediction = list(list(model.predict(board))[0])
        while True:
            move = prediction.index(max(prediction))
            try:
                result = game.play_turn_ai(move)
                break
            except IndexError:
                prediction[move] = -10  # Guarantee it will not be chosen again
        if result == Result.NOT_FINISHED:
            print(game.state)
            result = game.play_turn()
    print(game.state)
    print('Game Over!')
    print(result)


if __name__ == '__main__':
    main()
